# CPUCtl

A simple utility written in Nim to control the scaling governor through the CPUFreq `/sys` entries.
