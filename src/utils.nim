import osproc, strutils

const
    sys_entry_governor = "/sys/devices/system/cpu/cpu$1/cpufreq/scaling_governor"
    sys_entry_boost = "/sys/devices/system/cpu/cpufreq/boost"

let
    num_cpus = countProcessors()

proc writeToSys(file: string, content: string) = 
    assert len(file) > 0
    assert len(content) > 0
    try:
        writeFile(file, content)
    except IOError:
        echo "I/O error (usually a permissions issue)\nSystem message: ", getCurrentExceptionMsg()
    except:
        echo "Weird exception. What are you trying to do?\nSystem message: ", getCurrentExceptionMsg()

proc readFromSys(entry: string): string =
    assert len(entry) > 0
    var read: string
    try:
        read = readFile(entry)
    except IOError:
        echo "I/O error.\nSystem message: ", getCurrentExceptionMsg()
    except:
        echo "Weird exception. What are you trying to do?\nSystem message: ", getCurrentExceptionMsg()
    read

proc changeGovernor*(governor: string = "schedutil") =
    for n in 0..<num_cpus:
        writeToSys(sys_entry_governor % [n.intToStr], governor)

proc printGovernors*() =
    for n in 0..<num_cpus:
        echo "CPU", n, ": ", readFromSys(sys_entry_governor % [n.intToStr])

proc changeBoost*(enabled: bool) =
    var enable_boost = (if enabled: "1" else: "0")
    writeToSys(sys_entry_boost, enable_boost)

proc printBoost*() =
    var is_enabled = (if readFromSys(sys_entry_boost).contains("1"): "enabled" else: "disabled")
    echo "CPU boost: ", is_enabled