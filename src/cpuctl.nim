# This is just an example to get you started. A typical binary package
# uses this file as the main entry point of the application.
import parseopt, "utils.nim"

when isMainModule:

    for kind, key, val in getopt():
        case kind
        of cmdArgument: # val not used for CmdArgument
            case key
            of "schedutil", "ondemand", "conservative", "powersave", "performance":
                changeGovernor(key)
            of "enable-boost":
                changeBoost(true)
            of "disable-boost":
                changeBoost(false)
            else: echo "Invalid option. See --help, -h"
        of cmdLongOption, cmdShortOption:
            case key
            of "help", "h": 
                echo "Usage: cpuctl [GOVERNOR|enable-boost|disable-boost] [--option]"
                echo "Options are:\n --governor, -g for printing current scaling governor for each CPU"
                echo " --boost, -b for printing the CPU boost status"
                echo " --help, -h for printing this"
            of "governor", "g":
                printGovernors()
            of "boost", "b":
                printBoost()
        else: echo "Invalid option" 
