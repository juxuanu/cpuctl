# Package

version       = "0.0.1"
author        = "Ícar N. S."
description   = "Simple CPU scaling governor utility for CPUFreq"
license       = "GPL-2.0-only"
srcDir        = "src"
bin           = @["cpuctl"]


# Dependencies

requires "nim >= 1.4.8"
